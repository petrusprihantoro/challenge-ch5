const err400 = ({status="Error 400",message,data={}},res) => {
    res.status(401).json({
        status,
        message,
        data
    })
}

const err500 = (err,res) => {
    console.log(err);
    res.status(500).json({
        status:'Error 500',
        message:'Something wrong!',
        errors:err
    });
}

module.exports = {
    err400,
    err500
}
