const {Photo,User} = require('../models')
const Err = require('./error')

const createPhoto = async (req,res) => {
    try {
        let {user_id,caption} = req.body
    
        if(!user_id || !caption) {
            Err.err400({
                message:"Data isn\'t enough!"
            },res)
            return false
        }

        let owner = await User.findOne({
            where:{
                id:user_id
            }
        })

        if(!owner) {
            Err.err400({
                message:"User not found"
            },res)
            return
        }

        let photo = await Photo.create({
            user_id,
            caption
        })
    
        res.status(201).json({
            status:"Success",
            message:"Photo posted!",
            data:photo
        })
    } catch (err) {
        Err.err500(err,res)
    }
}

const getPhoto = async (req,res) => {
    try {
        let photos = await Photo.findAll({
            include:['owner','users_commented']
        })
    
        res.status(200).json({
            status:"Success!",
            message:"All photos successfully obtained!",
            data:photos
        })
    } catch (err) {
        Err.err500(err,res)
    }
}

const getPhotoDetail = async (req,res) => {
    try {
        let photo_id = req.params.id
    
        let photo = await Photo.findOne({
            where:{
                id:photo_id
            },
            include:['owner','users_commented']
        })
    
        if (!photo) {
            Err.err400({
                message:`Photo with id ${photo_id} doesn\'t exist!`
            })
            return false
        }
    
        res.status(200).json({
            status:"Succes",
            message:`Photo detail with id ${photo_id}`,
            data:photo
        })
    } catch (err) {
        Err.err500(err,res)
    }
}

const updatePhoto = async (req,res) => {
    try {
        let photo_id = req.params.id
        let {user_id,caption} = req.body

        let owner = await User.findOne({
            where:{
                id:user_id
            }
        })

        if(!owner) {
            Err.err400({
                message:"User not found"
            },res)
            return false
        }
    
        let updatedPhoto = await Photo.update({
            user_id,
            caption
        },{
            where:{
                id:photo_id
            }
        })
    
        if(!updatedPhoto) {
            Err.err400({
                message:`Photo with id ${photo_id} doesn\'t exist!`
            },res)
            return
        }
    
        res.status(201).json({
            status:"Succes",
            message:"Photo information updated!",
            data:updatedPhoto
        })
    } catch (err) {
        Err.err500(err,res)
    }
}

const deletePhoto = async (req,res) => {
    try {
        let photo_id = req.params.id
    
        let deletedPhoto = await Photo.destroy({
            where:{
                id:photo_id
            }
        })
    
        if(!deletedPhoto) {
            Err.err400({
                message:`Photo with id ${photo_id} doesn\'t exist!`
            },res)
            return false
        }
    
        res.status(201).json({
            status:"Succes",
            message:"Photo deleted!",
            data:deletedPhoto
        })
    } catch (err) {
        Err.err500(err,res)
    }
}

module.exports = {
    createPhoto,
    getPhoto,
    getPhotoDetail,
    updatePhoto,
    deletePhoto
}
