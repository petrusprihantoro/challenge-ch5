const {Comment,User} = require('../models')
const Err = require('./error')

const createComment = async (req,res) => {
    try {
        let {photo_id,user_id,comment} = req.body
    
        if(!photo_id || !user_id || !comment) {
            Err.err400({
                message:"Data isn\'t enough!"
            },res)
            return false
        }

        let owner = await User.findOne({
            where: {
                id:user_id
            }
        })

        if(!owner) {
            Err.err400({
                message:"User not found!"
            },res)
        }        

        let commentData = await Comment.create({
            photo_id,
            user_id,
            comment
        })
    
        res.status(201).json({
            status:"Success",
            message:"New comment created!",
            data:commentData
        })
    } catch (err) {
        Err.err500(err,res)
    }
}

const getComment = async (req,res) => {
    try {
        let comments = await Comment.findAll()
    
        res.status(200).json({
            status:"Success!",
            message:"All comment successfully obtained!",
            data:comments
        })
    } catch (err) {
        Err.err500(err,res)
    }
}

const getCommentDetail = async (req,res) => {
    try {
        let comment_id = req.params.id
    
        let comment = await Comment.findOne({
            where:{
                id:comment_id
            }
        })
    
        if (!comment) {
            Err.err400({
                message:`Comment with id ${comment_id} doesn\'t exist!`
            },res)
            return false
        }
    
        res.status(200).json({
            status:"Succes",
            message:`Comment detail with id ${comment_id}`,
            data:comment
        })
    } catch (err) {
        Err.err500(err,res)
    }
}

const updateComment = async (req,res) => {
    try {
        let comment_id = req.params.id
        let {photo_id,user_id,comment} = req.body
    
        let updatedComment = await Comment.update({
            photo_id,
            user_id,
            comment
        },{
            where:{
                id:comment_id
            }
        })
    
        if(!updatedComment) {
            Err.err400({
                message:`Comment with id ${comment_id} doesn\'t exist!`
            },res)
            return
        }
    
        res.status(201).json({
            status:"Succes",
            message:"Comment information updated!",
            data:updatedComment
        })
    } catch (err) {
        Err.err500(err,res)
    }
}

const deleteComment = async (req,res) => {
    try {
        let comment_id = req.params.id
    
        let deletedComment = await Comment.destroy({
            where:{
                id:comment_id
            }
        })
    
        if(!deletedComment) {
            Err.err400({
                message:`Comment with id ${comment_id} doesn\'t exist!`
            },res)
            return false
        }
    
        res.status(201).json({
            status:"Succes",
            message:"Comment deleted!",
            data:deletedComment
        })
    } catch (err) {
        Err.err500(err,res)
    }
}

module.exports = {
    createComment,
    getComment,
    getCommentDetail,
    updateComment,
    deleteComment
}
