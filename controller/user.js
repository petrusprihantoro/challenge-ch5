const {User,Photo} = require('../models')
const Err = require('./error')

const createUser = async (req,res) => {
    try {
        let {name,password} = req.body
    
        if(!name || !password) {
            Err.err400({
                message:"Data isn\'t enough!"
            },res)
            return false
        }
    
        let old_user = await User.findOne({
            where: {
                name
            }
        })
    
        if (old_user) {
            Err.err400({
                message:"User has existed!",
                data:old_user
            },res)
            return false
        }
    
        let user = await User.create({
            name,
            password
        })
    
        res.status(201).json({
            status:"Success",
            message:"New user created!",
            data:user
        })
    } catch (err) {
        Err.err500(err,res)
    }
}

const getUser = async (req,res) => {
    try {
        let users = await User.findAll({
            // include:['posted_photos','commented_photos']
            include :[{
                model: Photo,
                as: 'posted_photos',
                attributes: ['user_id','caption']
            },{
                model: Photo,
                as: 'commented_photos',
                attributes: ['user_id','caption']
            }]
        })
        
    
        res.status(200).json({
            status:"Success!",
            message:"All user successfully obtained!",
            data:users
        })
    } catch (err) {
        Err.err500(err,res)
    }
}

const getUserDetail = async (req,res) => {
    try {
        let user_id = req.params.id
    
        let user = await User.findOne({
            where:{
                id:user_id
            },
            include:['posted_photos','commented_photos']
        })
    
        if (!user) {
            Err.err400({
                message:`User with id ${user_id} doesn\'t exist!`
            },res)
            return false
        }
    
        res.status(200).json({
            status:"Succes",
            message:`User detail with id ${user_id}`,
            data:user
        })
    } catch (err) {
        Err.err500(err,res)
    }
}

const updateUser = async (req,res) => {
    try {
        let user_id = req.params.id
        let {name,password} = req.body
    
        let updatedUser = await User.update({
            name,
            password
        },{
            where:{
                id:user_id
            }
        })
    
        if(!updatedUser) {
            Err.err400({
                message:`User with id ${user_id} doesn\'t exist!`
            },res)
            return
        }
    
        res.status(201).json({
            status:"Succes",
            message:"User information updated!",
            data:updatedUser
        })
    } catch (err) {
        Err.err500(err,res)
    }
}

const deleteUser = async (req,res) => {
    try {
        let user_id = req.params.id
    
        let deletedUser = await User.destroy({
            where:{
                id:user_id
            }
        })
    
        if(!deletedUser) {
            Err.err400({
                message:`User with id ${user_id} doesn\'t exist!`
            },res)
            return false
        }
    
        res.status(201).json({
            status:"Succes",
            message:"User deleted!",
            data:deletedUser
        })
    } catch (err) {
        Err.err500(err,res)
    }
}

module.exports = {
    createUser,
    getUser,
    getUserDetail,
    updateUser,
    deleteUser
}
