const express = require('express')
const app = express()
const port = 3000

const router = require('./routes')
const morgan = require('morgan')
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

app.use(express.json());
app.use(morgan('dev'));
app.use('/api/v1',router)
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(port,()=> {
    console.log('Application is running in port 3000');
})