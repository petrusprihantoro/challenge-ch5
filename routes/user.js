const express = require('express')
const router = express.Router()
const {createUser,getUser,getUserDetail,updateUser,deleteUser} = require('../controller/user')

router.get('/',getUser);
router.get('/:id',getUserDetail);
router.post('/',createUser);
router.put('/:id',updateUser);
router.delete('/:id',deleteUser);

module.exports = router;