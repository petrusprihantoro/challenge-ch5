const express = require('express')
const router = express.Router()
const {createComment,getComment,getCommentDetail,updateComment,deleteComment} = require('../controller/comment')

router.get('/',getComment);
router.get('/:id',getCommentDetail);
router.post('/',createComment);
router.put('/:id',updateComment);
router.delete('/:id',deleteComment);

module.exports = router;