const express = require('express')
const router = express.Router()
const {createPhoto,getPhoto,getPhotoDetail,updatePhoto,deletePhoto} = require('../controller/photo')

router.get('/',getPhoto);
router.get('/:id',getPhotoDetail);
router.post('/',createPhoto);
router.put('/:id',updatePhoto);
router.delete('/:id',deletePhoto);

module.exports = router;