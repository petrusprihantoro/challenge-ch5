const express = require('express');
const router = express.Router();
const comment = require('./comment')
const photo = require('./photo')
const user = require('./user')

router.get('/', (req,res) => {
    res.status(200).json({
        status: 'Success',
        message: 'Welcome to INSTAGRAM-like API',
        data:null
    });
});

router.use('/comment',comment)
router.use('/user',user)
router.use('/photo',photo)

module.exports = router;