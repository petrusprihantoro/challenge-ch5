'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Photo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Photo.belongsTo(models.User, {foreignKey: 'user_id', as:'owner'})
      Photo.belongsToMany(models.User, {foreignKey: 'photo_id', as:'users_commented', through:models.Comment})
    }
  }
  Photo.init({
    user_id: DataTypes.BIGINT,
    caption: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Photo',
  });
  return Photo;
};